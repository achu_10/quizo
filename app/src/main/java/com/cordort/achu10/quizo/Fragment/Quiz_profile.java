package com.cordort.achu10.quizo.Fragment;

import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import com.cordort.achu10.quizo.R;


public class Quiz_profile extends Fragment {
    private Button button_play;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_quiz_profile, container, false);
        button_play = (Button) view.findViewById(R.id.button_play);
        return view;

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        button_play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                FragmentManager fm2=getFragmentManager();
                fm2.beginTransaction().replace(R.id.content_quiz_frame,new oponent_search())
                        .addToBackStack("my_stack").commit();
            }
        });
    }
}
