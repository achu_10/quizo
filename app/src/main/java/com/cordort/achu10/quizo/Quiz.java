package com.cordort.achu10.quizo;


import android.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import com.cordort.achu10.quizo.Fragment.Quiz_profile;
import com.cordort.achu10.quizo.Fragment.oponent_search;

public class Quiz extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        FragmentManager fm=getFragmentManager();
        fm.beginTransaction().replace(R.id.content_quiz_frame,new Quiz_profile())
                .addToBackStack("my_stack").commit();



    }
}
